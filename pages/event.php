<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$id = (int)$matches[1];

$event = Db::ExecuteFirst("SELECT * FROM event WHERE ID = $id", $conn);

Db::CloseConnection($conn);

$event["start_date"] = disDate($event["start_date"]);
$event["start_time"] = disTime($event["start_time"]);
$event["end_date"] = disDate($event["end_date"]);
$event["end_time"] = disTime($event["end_time"]);

$context["event"] = $event;

echo $twig->render('event.html', $context);