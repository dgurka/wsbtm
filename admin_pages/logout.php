<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

session_destroy();

redirect(URL_ROOT . "admin/");